import { join, dirname } from 'path'
import { fileURLToPath } from 'url'
import swaggerAutogen from 'swagger-autogen'

const _dirname = dirname(fileURLToPath(import.meta.url))

const doc = {
    info: {
      title: 'MEVN CRUD API',
      description: "Here is the API view of the task. It consists of two models: provider and client, as well as a full CRUD around them."
    },
    definitions: {
      Provider: {
        id: 1,
        name: 'Provider 1',
      },
      Providers: [
        {
          $ref: '#/definitions/Provider'
        }
      ],
      Client: {
        _id: '63446a86b268eff66840fcbf',
        name: 'Test',
        email: 'test@krfs.com',
        phone: '3055550000',
        providers: [
          {
            id: 1
          }
        ]
      },
      Clients: [
        {
          $ref: '#/definitions/Client'
        }
      ],
      StateObject: {
        state: 'Success'
      },
    },
    host: 'localhost:5000',
    basePath: '/api/v1',
    tags: [
      {
        'name': 'Providers',
        'description': 'Providers endpoints'
      },
      {
        'name': 'Clients',
        'description': 'Clients endpoints'
      }
    ],
    schemes: ['http']
}

const outputFile = join(_dirname, 'output.json')
const endpointsFiles = [join(_dirname, '../routes.js')]

swaggerAutogen(/*options*/)(outputFile, endpointsFiles, doc).then(({ success }) => {
 console.log(`Generated: ${success}`)
})