import express from 'express';
import { clientController } from './controllers/clients.controller.js';
import {providerController} from './controllers/providers.controller.js';

const router = express.Router();

router.route('/providers/:id?')
  .get(
    /*
    #swagger.path = '/providers'
    #swagger.tags = ['Providers']
    #swagger.description = 'Get all providers'
    #swagger.responses[200] = {
      description: 'Array of all providers',
      schema: { $ref: '#/definitions/Providers' }
    }
    #swagger.responses[400] = {
      description: 'Error object',
      schema: { $ref: '#/definitions/Error' }
    }
    */

   providerController.getProviders)
  .post(
    /*
    #swagger.path = '/providers'
    #swagger.tags = ['Providers']
    #swagger.description = 'Create new provider'
    #swagger.parameters['provider'] = {
      in: 'body',
      required: true,
      description: 'id is not required',
      schema: { $ref: '#/definitions/Provider' }
    }
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Created' }
    }
    #swagger.responses[400] = {
       description: 'Error object',
       schema: { $ref: '#/definitions/Error' }
    }
    */
    
    providerController.createProvider)
  .put(
    /*
    #swagger.tags = ['Providers']
    #swagger.description = 'Update provider'
    #swagger.parameters['name'] = {
      type: 'string',
      description: 'Name of the provider',
      reqired: true
    }
    
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Updated' }
    }
    #swagger.responses[400] = {
       description: 'Error object',
       schema: { $ref: '#/definitions/Error' }
    }
    */

    providerController.updateProvider)
  .delete(
    /*
    #swagger.tags = ['Providers']
    #swagger.description = 'Delete provider'
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Deleted' }
    }
    #swagger.responses[400] = {
      description: 'Error object',
      schema: { $ref: '#/definitions/Error' }
    }
    */

    providerController.deleteProvider)

router.route('/clients/:_id?')
  .get(
    /*
    #swagger.path = '/clients'
    #swagger.tags = ['Clients']
    #swagger.description = 'Get all clients'
    #swagger.responses[200] = {
      description: 'Array of all clients',
      schema: { $ref: '#/definitions/Clients' }
    }
    */
   
    clientController.getClients)
  .post(
    /*
    #swagger.path = '/clients'
    #swagger.tags = ['Clients']
    #swagger.description = 'Creates new client'
    #swagger.parameters['client'] = {
      in: 'body',
      description: '_id is not required',
      required: true,
      schema: { $ref: '#/definitions/Client' }
    }
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Created' }
    }
    #swagger.responses[400] = {
      description: 'Error object',
      schema: { $ref: '#/definitions/Error' }
    } */
    
    clientController.createClient)
  .put(
    /*
    #swagger.tags = ['Clients']
    #swagger.description = 'Update client'
    #swagger.parameters['client'] = {
      in: 'body',
      description: '_id is not required',
      required: true,
      schema: { $ref: '#/definitions/Client' }
    }
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Updated' }
    }
    #swagger.responses[400] = {
      description: 'Error object',
      schema: { $ref: '#/definitions/Error' }
    } */
    
    clientController.updateClient)
  .delete(
    /*
    #swagger.tags = ['Clients']
    #swagger.description = 'Delete client'
    #swagger.responses[200] = {
      description: 'Successful state object',
      schema: { state: 'Deleted' }
    }
    #swagger.responses[400] = {
      description: 'Error object',
      schema: { $ref: '#/definitions/Error' }
    } */
    
    clientController.deleteClient)

export default router;