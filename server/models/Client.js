import mongoose from "mongoose";

const {Schema} = mongoose;

const clientSchema = new Schema({
    name: {type: String,  required: true},
    email: {type: String, required: true},
    phone: {type: String, required: true},
    providers: [{type: Object}]
})

export default mongoose.model('Client', clientSchema);