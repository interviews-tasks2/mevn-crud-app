import mongoose from "mongoose";

const {Schema} = mongoose;

const ProviderSchema = new Schema({
    id: {type: Number, required: true},
    name: {type: String,  required: true},
})

export default mongoose.model('Provider', ProviderSchema);