
import Provider from '../models/Provider.js'
import Client from '../models/Client.js'

var maxId;
Provider.find().sort({id: -1}).limit(1).then(provider => maxId = provider[0]?.id ?? 0);

class ProviderService {
  
  async getAll() {
    return await Provider.find();
  }

  async create(name) {
      const provider = new Provider({id: maxId + 1, name: name});
      await provider.save();
      maxId++;
      return {state: 'Created'}
  }

  async update(id, provider) {
      await Provider.findOneAndUpdate({id: id}, provider);
      return {state: 'Updated'}
  }

  async delete(id) {
      await Client.updateMany({ }, {$pull: { 'providers': { id: Number(id)}}});
      await Provider.findOneAndRemove({id: id});
      return {state: 'Deleted'}
  }
}

export let providerService = new ProviderService();