import Client from '../models/Client.js'
class ClientService {
  async getAll() {
      return await Client.find();
  }
  
  async create(name, email, phone, providers) {
      const client = new Client({name, email, phone, providers});
      await client.save();
      return {state: 'Created'}
  }

  async update(_id, client) {
      await Client.findByIdAndUpdate(_id, client);
      return {state: 'Updated'}
  }

  async delete(_id) {
      await Client.findByIdAndRemove(_id);
      return {state: 'Deleted'}
  }
}

export let clientService = new ClientService();