export class ErrorCode {
    static NotFound = 'NotFound';
    static ValidateError = 'ValidateError';
    static AsyncError = 'AsyncError';
    static UnknownError = 'UnknownError';
  }
  
export class ErrorException extends Error {
  metaData = null;
  message = null;
  status = null;
  constructor(code = ErrorCode.UnknownError, message = null, metaData = null) {
      super(code);
      Object.setPrototypeOf(this, new.target.prototype);
      this.name = code;
      this.status = 500;
      this.message = message;
      this.metaData = metaData;
      switch (code) {
      case ErrorCode.ValidateError:
          this.status = 400;
          break;
      case ErrorCode.AsyncError:
          this.status = 400;
          break;
      case ErrorCode.NotFound:
          this.status = 404;
          break;
      default:
          this.status = 500;
          break;
      }
  }
}

export const errorsHandler = (err, req, res, next) => {
  if (err instanceof ErrorException) {
    res.status(err.status).send(err);
  } else {
    res.status(500).send({ code: ErrorCode.UnknownError, status: 500 });
  }
};