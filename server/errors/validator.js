import { ErrorException, ErrorCode } from '../errors/errors-handler.js';

export function validationRequired(src, ...args) {
  args.forEach(arg => {
    if (!src.hasOwnProperty(arg) || src[arg] === null || src[arg] === undefined) {
      throw new ErrorException(ErrorCode.ValidateError, `Field ${arg} is required!` )
    }
  });
}