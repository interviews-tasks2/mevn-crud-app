import { providerService } from '../services/providers.service.js';
import { validationRequired } from '../errors/validator.js'

class ProviderController {
  async getProviders(req, res, next) {
    try {
      res.json(await providerService.getAll());
    } catch (e) {
      next(e);
    }
  }

  async createProvider(req, res, next) {
    try {
      validationRequired(req.body, 'name')
      res.json(await providerService.create(req.body.name));
    } catch (e) {
      next(e);
    }
  }

  async updateProvider(req, res, next) {  
    try {
      validationRequired(req.params, 'id')
      validationRequired(req.body, 'name')

      const provider = { id: req.params.id, name: req.body.name}
      res.json(await providerService.update(provider.id, provider));
    } catch (e) {
      next(e);
    }
  }

  async deleteProvider(req, res, next) {
    try {
      validationRequired(req.params, 'id')

      res.json(await providerService.delete(Number(req.params.id)));
    } catch (e) {
      next(e);
    }
  }
}

export let providerController = new ProviderController();
