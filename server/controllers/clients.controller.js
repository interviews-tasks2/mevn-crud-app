import { clientService } from '../services/clients.service.js';
import { validationRequired } from '../errors/validator.js'


class ClientController {
  async getClients(req, res, next) {
    try {
      res.json(await clientService.getAll());
    } catch (e) {
      next(e)
    }
  }

  async createClient(req, res, next) {
    try {
      const b = req.body;
      validationRequired(b, 'name', 'email', 'phone')
      res.json(await clientService.create(b.name, b.email, b.phone, b.providers));
    } catch (e) {
      next(e);
    }
  }

  async updateClient(req, res, next) {
    try {
      const b = req.body;
      validationRequired(req.params, '_id')
      validationRequired(b, 'name', 'email', 'phone')

      res.json(await clientService.update(
        req.params._id,
        {
          name: b.name,
          email: b.email,
          phone: b.phone,
          providers: b.providers
        }
      ));
    } catch (e) {
      next(e);   
    }
  }

  async deleteClient(req, res, next) {
    try {
      validationRequired(req.params, '_id')

      res.json(await clientService.delete(req.params._id));
    } catch (e) {
      next(e);
    }
  }
}

export let clientController = new ClientController();
