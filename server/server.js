import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import morgan from "morgan";
import fs from 'fs'
import swaggerUi from 'swagger-ui-express'

import routes from "./routes.js";
import { errorsHandler } from "./errors/errors-handler.js";

const app = express();

const allowedOrigins = ['http://127.0.0.1:5173'];


app.set('port', 5000);
app.use(
  cors({
    origin: allowedOrigins,
  })     
);
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(morgan('dev'));

app.use('/api/v1', routes)
app.use(errorsHandler)

const swaggerFile = JSON.parse(fs.readFileSync('./server/swagger/output.json'))
app.use('/api/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile))

mongoose.connect('mongodb://127.0.0.1:27017/mevn-crud', { useNewUrlParser: true })
  .then(db => {
    app.listen(app.get('port'), () => {
      console.log(`[OK] 🚀 Server is running on http://localhost:${app.get('port')}`);
      console.log(`[OK] 📖 API Documentation is running on http://localhost:${app.get('port')}/api/doc`);
      console.log('[OK] 💾 DB is connected')
    })
  })
  .catch(err => console.error(err));
 


