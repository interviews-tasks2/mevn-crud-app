/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        default: "#8c8e8c",
        header: "#295d73",
        link: "#739aa5",
        border: "#dedbde",
        icons: "#5d5e5d",
        secondary: "#f4f4f4",
        primary: "#f2f6fa",
        danger: "#f7494a"
      }
    },
  },
  plugins: [],
}
