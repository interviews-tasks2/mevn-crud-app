# MEVN CRUD App

This is the test application, which demonstrates standard CRUD operations around MEVN stack.

## Dependencies
- [Node + Npm](https://docs.npmjs.com/cli/v8/configuring-npm/install/) 
- [MongoDB](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/) 

## How to install
1. Clone repository via command:
```bash
git clone https://gitlab.com/interviews-tasks2/mevn-crud-app.git
```

2. Then navigate to the root of the downloaded folder and run:
```bash
npm install
```

## How to start server
1. In the root of the project run:
```bash
npm run back:dev
npm run front:dev
```
Application will be running on `5173` port and API server on `5000` port.

You can learn more about API Methods on [Swagger Docs](http://127.0.0.1:5000/api/doc) (only when locally hosted)

## How to build project
```bash
npm run build
```
