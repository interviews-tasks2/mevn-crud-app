import api from "./axios-base";

export const get = async () => {
  try {
      const res = await api.get("/clients");
      return res.data;
    } catch (e) {
      console.error(e.name + "\n" + e.message);
      if (e.response.data)
          throw e.response.data
      throw e;
    }
}

export const create = async (client) => {
  try {
      const res = await api.post("/clients", client)
      return res.data;
  } catch (e) {
      console.error(e.name + "\n" + e.message);
      if (e.response.data)
        throw e.response.data
      throw e;
  }
}

export const update = async (id, client) => {
  try {
      await api.put(`/clients/${id}`, client)
  } catch (e) {
    console.error(e.name + "\n" + e.message);
    if (e.response.data)
      throw e.response.data
    throw e;
  }
}

export const remove = async (id) => {
  try {
      await api
      .delete(`/clients/${id}`)
  } catch (e) {
    console.error(e.name + "\n" + e.message);
    if (e.response.data)
      throw e.response.data
    throw e;
  }
}

