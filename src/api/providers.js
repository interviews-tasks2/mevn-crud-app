import api from "./axios-base";

export const get = async () => {
  try {
      const res = await api.get("/providers");
      return res.data;
    } catch (e) {
      console.error(e.name + "\n" + e.message);
      if (e.response.data)
        throw e.response.data
      throw e;
    }
}

export const create = async (name) => {
  try {
      const res = await api
      .post("/providers", {
        name: name
      })
      return res.data;
  } catch (e) {
    console.error(e.name + "\n" + e.message);
    if (e.response.data)
      throw e.response.data
    throw e;
  }
}

export const update = async (id, name) => {
  try {
      await api
      .put(`/providers/${id}`, {name: name})
  } catch (e) {
    console.error(e.name + "\n" + e.message);
    if (e.response.data)
      throw e.response.data
    throw e;
  }
}

export const remove = async (id) => {
  try {
      await api
      .delete(`/providers/${id}`)
  } catch (e) {
    console.error(e.name + "\n" + e.message);
    if (e.response.data)
      throw e.response.data
    throw e;
  }
}

